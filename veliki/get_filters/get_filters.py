import ipdb
import json
import grequests
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup
from datetime import datetime
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def clickedHtml(url):
    driver = webdriver.Chrome()
    driver.get(url)
    elem = driver.find_elements_by_class_name("show-more")
    for button in elem:
        button.click()
    
    time.sleep(2)
    html = driver.page_source
    driver.close()
    
    return html

def get_pagination_quantity(url):
    req = requests.get(url)
    soup = BeautifulSoup(req.text, 'html.parser')
    pages = soup.find("div", {"class": "pages"})
    
    if pages:
        pagination_block_soup = pages.find_all('a', href=True)
        
        last = pagination_block_soup[-1]
        
        if ( 'title' not in last.attrs ):
            pagination = int(url.split('=').pop())
            return pagination
        else:
            return get_pagination_quantity(pagination_block_soup[-2].attrs['href'])
    else:
        return 0

def get_filter_blocks(soup):
    soup = soup.find("dl", {"id": "narrow-by-list"})

    filter_blocks_soup_name = soup.find_all('dt')
    filter_blocks_soup_hrefs = soup.find_all('dd')

    filter_blocks = {}
    tmp = []
    counter = 0
    
    for name in filter_blocks_soup_name:
        block = filter_blocks_soup_hrefs[counter]
        
        hrefs = block.find_all('a', href=True)
        
        if hrefs:
            tmp = [href['href'] for href in hrefs]
            counter += 1
        else:
            counter += 1
            continue
            
        filter_blocks[name.text] = tmp
                        
    return filter_blocks

def get_current_active_filter_block(filter_blocks, name):
    for block in filter_blocks:
        if name in block.keys():
            return filter_blocks.index(block)

def get_items_quantity(url):
    req = requests.get(url)
    
    time.sleep(1)
    
    soup = BeautifulSoup(req.text, 'html.parser')
    
    soup = soup.find("div", {"class": "category-products"})
    soup = soup.find("ul")
    items = soup.find_all('li')

    return len(items)

def get_category_list(url):
    try:
        req = requests.get(url)
        
        time.sleep(1)
        
        soup = BeautifulSoup(req.text, 'html.parser')
        soup = soup.find("ul", {"class": "category-list"})
        hrefs = soup.find_all('a', href=True)

        return hrefs
    except:
        return False
    
def get_info(url):
    req = requests.get(url)
    
    try:
        h1_pattern = "<h1[^>]*>(.*?)<\/h1>"
        title_pattern = "<title[^>]*>(.*?)<\/title>"
        
        text = str(req.text)
        text = text.replace('\t', '')
        text = text.replace('\n', '')
        
        h1 = re.findall(h1_pattern, text)
        title = re.findall(title_pattern, text)
        
        return [h1[0], url, req, title[0]]
    except:
        pass
    
def get_all_filters(one_filter_soup):
    urls_blocks = get_filter_blocks(one_filter_soup)
    # one_urls_block = get_filter_sitemap(one_filter_blocks, 'one')
    keys = {'one_filter_data':[], 'two_filter_data':[]}
    
    for key in urls_blocks.keys():
        for of_url in urls_blocks[key]:
            print(of_url)
            
            items_quantity = get_items_quantity(of_url)
            
            if (items_quantity > 3):
                one_filter_data = get_info(of_url)
                
                tmp_base = {}
                tmp_base['h1'] = one_filter_data[0]
                tmp_base['url'] = one_filter_data[1]
                tmp_base['title'] = one_filter_data[3]
                tmp_base['pagination'] = get_pagination_quantity(of_url)
                
                keys['one_filter_data'].append(tmp_base)
                print("one_filters_add:", of_url)
                
                two_filter_soup = BeautifulSoup(one_filter_data[2].text, 'html.parser')
                two_filter_blocks = get_filter_blocks(two_filter_soup)
                
                for key_two in dict(two_filter_blocks):
                    if key == key_two:
                        two_filter_blocks.pop(key_two)
                        break
                    two_filter_blocks.pop(key_two)
                
                for key_two in two_filter_blocks.keys():
                    for tf_url in two_filter_blocks[key_two]:
                        print(tf_url)
                        
                        items_quantity_two = get_items_quantity(tf_url)
                        
                        if ( items_quantity_two > 3 ):
                            two_filter_data = get_info(tf_url)
                            
                            tmp = {}
                            tmp['h1'] = two_filter_data[0]
                            tmp['url'] = two_filter_data[1]
                            tmp['title'] = two_filter_data[3]
                            tmp['pagination'] = get_pagination_quantity(tf_url)
                            
                            keys['two_filter_data'].append(tmp)
                            print("two_filters_add:", tf_url)
                        else:
                            print("two_filter_empty:", tf_url)
            else:
                print("one_filter_empty:", of_url)
                
                    
    print("return time: ")
    print(datetime.now())
    return keys


print("Time start script:")
print(datetime.now())


if ( __name__ == '__main__' ):
    urls = ["https://shop.veliki.ua/otdyh-i-turizm.html"]
    
    for url in urls:
        # Create base folder for category
        filepath = url.split('?')
        filepath = filepath[0].split('/')
        filepath = filepath.pop().split('.')
        filepath = 'files/' + filepath.pop(0) + '/'
        
        if not os.path.isdir(filepath):
            os.mkdir(filepath)
            
        base_req = requests.get(url)
        
        base_soup = BeautifulSoup(base_req.text, 'html.parser')
        
        base_categories_keys = get_all_filters(base_soup)

        with open(filepath + 'base_one_filters.csv', 'w') as file:
            fildnames = ['h1', 'title', 'url', 'pagination']
            w = csv.DictWriter(file, fildnames)
            w.writeheader()
            w.writerows(base_categories_keys['one_filter_data'])

        with open(filepath + 'base_two_filters.csv', 'w') as file:
            fildnames = ['h1', 'title', 'url', 'pagination']
            w = csv.DictWriter(file, fildnames)
            w.writeheader()
            w.writerows(base_categories_keys['two_filter_data'])

        base_categories = get_category_list(url)
        if base_categories:
            sub_categories = [url['href'] for url in base_categories]
            sub_categories_keys = {}

            for category in sub_categories:
                if 'javascript:void(0)' not in category:
                    filename = category.split('/')
                    filename = filename.pop().split('.')[0]
                    
                    sub_path = filepath + filename
                    
                    if not os.path.isdir(sub_path):
                        os.mkdir(sub_path)
                    
                    sub_req = requests.get(category)
                    
                    time.sleep(2)
                    
                    sub_soup = BeautifulSoup(sub_req.text, 'html.parser')
                    
                    filename_one = sub_path+'/base_one_filter.csv'
                    filename_two = sub_path+'/base_two_filter.csv'

                    sub_keys = get_all_filters(sub_soup)
                    
                    if sub_keys:
                        with open(filename_one, 'w') as file:
                            fildnames = ['h1', 'title', 'url', 'pagination']
                            w = csv.DictWriter(file, fildnames)
                            w.writeheader()
                            w.writerows(sub_keys['one_filter_data'])
                            
                        with open(filename_two, 'w') as file:
                            fildnames = ['h1', 'title', 'url', 'pagination']
                            w = csv.DictWriter(file, fildnames)
                            w.writeheader()
                            w.writerows(sub_keys['two_filter_data'])
                        
                        sub_sub_soup = get_category_list(category)
                        
                        if sub_sub_soup:
                            end_categories = [url['href'] for url in sub_sub_soup]
                        
                            end_categories_keys = {}
                            for end_category in end_categories:
                                filename_end = end_category.split('/')
                                filename_end = filename_end.pop().split('.')[0]
                                
                                end_req = requests.get(end_category)
                                end_soup = BeautifulSoup(end_req.text, 'html.parser')
                                end_keys = get_all_filters(end_soup)
                                # end_req.
                                
                                filename_one = sub_path+'/'+filename_end+'_one_filter.csv'
                                filename_two = sub_path+'/'+filename_end+'_two_filter.csv'
                                
                                with open(filename_one, 'w') as file:
                                    fildnames = ['h1', 'title', 'url', 'pagination']
                                    w = csv.DictWriter(file, fildnames)
                                    w.writeheader()
                                    w.writerows(end_keys['one_filter_data'])
                                    
                                with open(filename_two, 'w') as file:
                                    fildnames = ['h1', 'title', 'url', 'pagination']
                                    w = csv.DictWriter(file, fildnames)
                                    w.writeheader()
                                    w.writerows(end_keys['two_filter_data'])

                print("Time end script:")
                print(datetime.now())


