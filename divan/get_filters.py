import ipdb
import json
# import grequests
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup
from datetime import datetime
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def clickedHtml(url):
    driver = webdriver.Chrome()
    driver.get(url)
    time.sleep(2)
    elem = driver.find_elements_by_tag_name("button")

    for button in elem:
        if "Показать все" in button.text :
            try:
                button.click()
                time.sleep(2)
            except:
                ipdb.set_trace()
    
    time.sleep(2)
    html = driver.page_source
    driver.close()
    
    return html

def get_pagination_quantity(url):
    req = requests.get(url)
    soup = BeautifulSoup(req.text, 'html.parser')
    pages = soup.find("div", {"class": "pages"})
    
    if pages:
        pagination_block_soup = pages.find_all('a', href=True)
        
        last = pagination_block_soup[-1]
        
        if ( 'title' not in last.attrs ):
            pagination = int(url.split('=').pop())
            return pagination
        else:
            return get_pagination_quantity(pagination_block_soup[-2].attrs['href'])
    else:
        return 0

def get_filter_blocks(soup):
    filters = []
    blocks = soup.find_all('div', {'class': 'filter__checks check-filter ocf-option-values'})

    if soup:
        for block in blocks:
            block = block.find_all('a')
            
            hrefs = [href['href'] for href in block]
            filters = filters + hrefs
        
        return filters
    else:
        return False


def get_category_list(url):
    req = requests.get(url)
        
    time.sleep(1)
    
    soup = BeautifulSoup(req.text, 'html.parser')
    
    base_categories = soup.find('ul', {'class': 'left-menuheader__list'})
    base_categories = base_categories.find_all('a', {'class': 'left-menuheader__link'})
    sub_categories = soup.find_all('a', {'class': 'right-menuheader__link'})
    
    urls = base_categories + sub_categories
    
    hrefs = ['https://divan.com.ua/' + href['href'].lstrip('/') for href in urls]

    return hrefs

    
def get_info(url):
    req = requests.get(url)
    
    try:
        h1_pattern = "<h1[^>]*>(.*?)<\/h1>"
        # title_pattern = "<title[^>]*>(.*?)<\/title>"
        # pagination_pattern = "[(]всего (.*?) страниц"
        
        text = str(req.text)
        text = text.replace('\t', '')
        text = text.replace('\n', '')
        
        h1 = re.findall(h1_pattern, text)[0]
        # title = re.findall(title_pattern, text)[0]
        # pagination = int(re.findall(pagination_pattern, text)[0])

        return [h1, url, req]
    except:
        pass
    
def get_all_filters(one_filter_soup):
    urls_blocks = get_filter_blocks(one_filter_soup)
    
    if urls_blocks:
        keys = {'one_filter_data':[]}
        
        for url in urls_blocks:
            print(url)
            try:
                one_filter_data = get_info(url)
            except:
                one_filter_data = get_info(url)
                
            if one_filter_data:
                tmp_base = {}
                tmp_base['h1'] = one_filter_data[0]
                tmp_base['url'] = one_filter_data[1]

                
                keys['one_filter_data'].append(tmp_base)
                print("one_filters_add:", url)
                pprint(tmp_base)
            else:
                print('No_data')
                    
                        
        print("return time: ")
        print(datetime.now())
        return keys
    else:
        return False


print("Time start script:")
print(datetime.now())


if ( __name__ == '__main__' ):
    urls = get_category_list("https://divan.com.ua/")
    
    for url in urls:
        # ipdb.set_tr ace()
        # Create base folder for category
        filepath = url.split('/')
        filepath = filepath.pop()
        filepath = 'files/' + filepath
        

        if not os.path.isfile(filepath + '.csv'):
            base_req = requests.get(url)

            time.sleep(1)
            
            base_soup = BeautifulSoup(base_req.text, 'html.parser')
            
            base_categories_keys = get_all_filters(base_soup)
            
            if base_categories_keys:
                with open(filepath + '.csv', 'w') as file:
                    fildnames = ['h1','url']
                    w = csv.DictWriter(file, fildnames)
                    w.writeheader()
                    w.writerows(base_categories_keys['one_filter_data'])
            else:
                continue

        # base_categories = get_category_list(url)
        # if base_categories:
        #     sub_categories = [url['href'] for url in base_categories]
        #     sub_categories_keys = {}

        #     for category in sub_categories:
        #         if 'javascript:void(0)' not in category:
        #             filename = category.split('/')
        #             filename = filename.pop().split('.')[0]
                    
        #             sub_path = filepath + filename
                    
        #             if not os.path.isdir(sub_path):
        #                 os.mkdir(sub_path)
                    
        #             sub_req = requests.get(category)
                    
        #             time.sleep(2)
                    
        #             sub_soup = BeautifulSoup(sub_req.text, 'html.parser')
                    
        #             filename_one = sub_path+'/base_one_filter.csv'
        #             filename_two = sub_path+'/base_two_filter.csv'

        #             sub_keys = get_all_filters(sub_soup)
                    
        #             if sub_keys:
        #                 with open(filename_one, 'w') as file:
        #                     fildnames = ['h1', 'title', 'url', 'pagination']
        #                     w = csv.DictWriter(file, fildnames)
        #                     w.writeheader()
        #                     w.writerows(sub_keys['one_filter_data'])
                            
        #                 with open(filename_two, 'w') as file:
        #                     fildnames = ['h1', 'title', 'url', 'pagination']
        #                     w = csv.DictWriter(file, fildnames)
        #                     w.writeheader()
        #                     w.writerows(sub_keys['two_filter_data'])
                        
        #                 sub_sub_soup = get_category_list(category)
                        
        #                 if sub_sub_soup:
        #                     end_categories = [url['href'] for url in sub_sub_soup]
                        
        #                     end_categories_keys = {}
        #                     for end_category in end_categories:
        #                         filename_end = end_category.split('/')
        #                         filename_end = filename_end.pop().split('.')[0]
                                
        #                         end_req = requests.get(end_category)
        #                         end_soup = BeautifulSoup(end_req.text, 'html.parser')
        #                         end_keys = get_all_filters(end_soup)
        #                         # end_req.
                                
        #                         filename_one = sub_path+'/'+filename_end+'_one_filter.csv'
        #                         filename_two = sub_path+'/'+filename_end+'_two_filter.csv'
                                
        #                         with open(filename_one, 'w') as file:
        #                             fildnames = ['h1', 'title', 'url', 'pagination']
        #                             w = csv.DictWriter(file, fildnames)
        #                             w.writeheader()
        #                             w.writerows(end_keys['one_filter_data'])
                                    
        #                         with open(filename_two, 'w') as file:
        #                             fildnames = ['h1', 'title', 'url', 'pagination']
        #                             w = csv.DictWriter(file, fildnames)
        #                             w.writeheader()
        #                             w.writerows(end_keys['two_filter_data'])

        #         print("Time end script:")
        #         print(datetime.now())


