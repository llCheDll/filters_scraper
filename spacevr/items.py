import ipdb
import json
import grequests
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup
from datetime import datetime
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def get_info(req):
    h1_pattern = "<h1[^>]*>(.*?)<\/h1>"
    meta_robots_pattern = "<meta[^>]*?name=[\'\"]?robots[\'\"] content=[\'\"](.*)*?[\'\"]>"
    
    text = str(req.text)
    text = text.replace('\t', '')
    text = text.replace('\n', '')
    meta_robots = re.findall(meta_robots_pattern, text)
    h1 = re.findall(h1_pattern, text)
    
    if(meta_robots):
        meta_robots = meta_robots[0]
    else:
        meta_robots = '-'
    
    return {'h1':h1[0], 'url':req.url, 'robots':meta_robots}

def get_items_urls(soup):
    soup = soup.find("section", {"class": "main-box"})
    soup = soup.find_all("button", href=True)
    
    item_urls = []
    
    for button in soup:
        item_urls.append(button['href'])
    
    return item_urls

def get_categories(soup):
    soup = soup.find("ul", {"class": "menu"})
    soup = soup.find_all("a", href=True)
    
    categories = [href['href'] for href in soup]
    
    return categories


if ( __name__ == '__main__' ):
    urls = ["https://www.spacevr.com.ua/"]
    items_urls = [
        "https://www.spacevr.com.ua/category/gamevr/page/",
        "https://www.spacevr.com.ua/category/questsvr/page/",
        "https://www.spacevr.com.ua/category/events/page/",
        "https://www.spacevr.com.ua/category/arenda-oborudovaniya/page/",
        "https://www.spacevr.com.ua/category/podarochnyye-sertifikaty/page/"
    ]
    
    coumter = 1
    
    info = []
    
    url = urls[0]
    
    filepath = 'files/'
        
    base_req = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    base_soup = BeautifulSoup(base_req.text, 'html.parser')
    
    # categories = get_categories(base_soup)
    
    # info.append(get_info(base_req))
    
    # for cat in categories:
    #     cat_req = requests.get(cat, headers={'User-Agent': 'Mozilla/5.0'})
    #     cat_soup = BeautifulSoup(base_req.text, 'html.parser')
        
    #     info.append(get_info(cat_req))

    # with open('files/items.csv', 'w') as file:
    #     fildnames = ['h1', 'url', 'robots']
    #     w = csv.DictWriter(file, fildnames)
    #     w.writeheader()
    #     w.writerows(info)
    
    for item in items_urls:
        counter = 1
        scraped_urls = []
        
        for i in range(0, 10):
            url = item + str(counter) + '/'
            print("HEND_URL:" + url)
            item_req = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
            
            counter += 1
            
            page = item_req.url.split('/')
            page.pop()
            page = page.pop()
            
            if page != str(counter) and item_req.url not in scraped_urls:
                print("SOURCE_URL:"+item_req.url)
                
                item_soup = BeautifulSoup(item_req.text, 'html.parser')
                
                hrefs = get_items_urls(item_soup)
                
                for href in hrefs:
                    href_req = requests.get(href, headers={'User-Agent': 'Mozilla/5.0'})
                    inf = get_info(href_req)
                    info.append(inf)
                    
                scraped_urls.append(item_req.url)

                with open('files/items.csv', 'w') as file:
                    fildnames = ['h1', 'url', 'robots']
                    w = csv.DictWriter(file, fildnames)
                    w.writeheader()
                    w.writerows(info)
            else:
                counter = 0
                break
                
            if not counter:
                break
    
    print(info)
    
    print("Time end script:")
    print(datetime.now())


