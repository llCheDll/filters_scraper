import ipdb
import json
import grequests
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup, Comment
from datetime import datetime
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def clickedHtml(url):
    driver = webdriver.Chrome()
    driver.get(url)
    elem = driver.find_elements_by_class_name("show-more")
    for button in elem:
        button.click()
    
    time.sleep(2)
    html = driver.page_source
    driver.close()
    
    return html

def get_pagination_quantity(soup):
    pages = soup.find('ul', {'class': 'pagin'})
    
    if pages:
        pages = pages.find_all('li')
        return pages[-2].text
    else:
        return 0

def get_filter_blocks(soup):
    # try:
    exclude_filter_block = ['Розничная цена', 'Постоянный клиент', 'Наши предложения', 'Наличие', 'Хит продаж' ]
    filter_blocks = soup.find_all('div', {"class": 'paramsBox'})
    
    result = {}
    
    for filter_block in filter_blocks:
        name = filter_block.find('div', {'class': 'paramsBoxTitle'}).text
        name = name.strip()
        if name not in exclude_filter_block:
            checkbox = filter_block.find('ul', {'class': 'checkbox'})
            hrefs = checkbox.find_all('a', href=True)
            result[name] = [href['href'] for href in hrefs]
    
    return result

def get_current_active_filter_block(filter_blocks, name):
    for block in filter_blocks:
        if name in block.keys():
            return filter_blocks.index(block)

def get_items_quantity(soup):
    try:
        items = soup.find('div', {'class': 'items productList'})
        items = items.find_all('div', {'class': 'item product sku'})
        return len(items)
    except:
        return 0
    
def get_category_list(url):
    try:
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        
        req = requests.get(url, headers=headers)
        
        time.sleep(1)
        
        soup = BeautifulSoup(req.text, 'html.parser')
        soup = soup.find('ul', {'id': 'leftMenu'})
        hrefs = soup.find_all('a', href=True)

        result = []
        
        for href in hrefs:
            href_tmp = href['href']
            if href_tmp not in result:
                result.append(href_tmp)
        
        return result
    except:
        return False
        
# def get_category_list(url):
    try:
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        
        req = requests.get(url, headers=headers)
        
        time.sleep(1)
        
        soup = BeautifulSoup(req.text, 'html.parser')
        soup = soup.find('div', {'id': 'nextSection'})
        hrefs = soup.find_all('span', {'class': 'sectionColumn'})
        hrefs = [href.a['href'] for href in hrefs]
        
        return hrefs
    except:
        return False
    
def get_info(req):
    h1_pattern = "<h1[^>]*>(.*?)<\/h1>"

    text = str(req.text)
    text = text.replace('\t', '')
    text = text.replace('\n', '')
    
    h1 = re.findall(h1_pattern, text)[0]
    
    return [h1, req.url, req]
    
def get_all_filters(one_filter_soup):
    urls_blocks = get_filter_blocks(one_filter_soup)
    
    keys = { 'one_filter_data':[], 'two_filter_data':[] }
    
    if urls_blocks:
        for key in urls_blocks.keys():
            for of_url in urls_blocks[key]:
                if of_url.split('/').pop() != '#':
                    print(of_url)
                    
                    req = requests.get(of_url, headers=headers)
                    time.sleep(1)
                    soup = BeautifulSoup(req.text, 'html.parser')

                    items_quantity = get_items_quantity(soup)
                    
                    # print('url: '+of_url+' items: '+str(items_quantity))
                    if (items_quantity >= 3):
                        one_filter_data = get_info(req)
                        
                        tmp_base = {}
                        tmp_base['h1'] = one_filter_data[0]
                        tmp_base['url'] = one_filter_data[1]
                        print(tmp_base)
                        keys['one_filter_data'].append(tmp_base)
                        print("one_filters_add:", of_url)
                        
                        two_filter_soup = BeautifulSoup(one_filter_data[2].text, 'html.parser')
                        two_filter_blocks = get_filter_blocks(two_filter_soup)
                        
                        for key_two in dict(two_filter_blocks):
                            if key == key_two:
                                two_filter_blocks.pop(key_two)
                                break
                            two_filter_blocks.pop(key_two)
                        
                        for key_two in two_filter_blocks.keys():
                            for tf_url in two_filter_blocks[key_two]:
                                if tf_url.split('/').pop() != '#':
                                    print(tf_url)
                                    
                                    tf_req = requests.get(tf_url, headers=headers)
                                    time.sleep(1)
                                    tf_soup = BeautifulSoup(tf_req.text, 'html.parser')
                                    
                                    items_quantity_two = get_items_quantity(tf_soup)
                                    
                                    if ( items_quantity_two >= 3 ):
                                        two_filter_data = get_info(tf_req)
                                        
                                        tmp = {}
                                        tmp['h1'] = two_filter_data[0]
                                        tmp['url'] = two_filter_data[1]
                                        
                                        print(tmp)
                                        
                                        keys['two_filter_data'].append(tmp)
                                        print("two_filters_add:", tf_url)
                                    else:
                                        print("two_filter_empty:", tf_url)
                else:
                    print("one_filter_empty:", of_url)
    else:
        return False
                
                    
    print("return time: ")
    print(datetime.now())
    return keys


print("Time start script:")
print(datetime.now())


if ( __name__ == '__main__' ):
    url = 'https://jcs.ua'
    
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    
    filepath = 'files'
    
    base_categories = get_category_list(url)
    pprint(base_categories)
    
    for category in base_categories:
        if '/' in category:
            full_url = url + category
            filename = category
            
            sub_path = filepath + filename
            
            if not os.path.isdir(sub_path):
                os.mkdir(sub_path)
                
            sub_req = requests.get(full_url, headers=headers)
            
            time.sleep(2)
            
            sub_soup = BeautifulSoup(sub_req.text, 'html.parser')
            
            filename_one = sub_path+'base_one_filter.csv'
            filename_two = sub_path+'base_two_filter.csv'

            sub_keys = get_all_filters(sub_soup)
            
            if sub_keys:
                with open(filename_one, 'w') as file:
                    fildnames = ['h1', 'url']
                    w = csv.DictWriter(file, fildnames)
                    w.writeheader()
                    w.writerows(sub_keys['one_filter_data'])
                    
                with open(filename_two, 'w') as file:
                    fildnames = ['h1', 'url']
                    w = csv.DictWriter(file, fildnames)
                    w.writeheader()
                    w.writerows(sub_keys['two_filter_data'])
                

            print("Time end script:")
            print(datetime.now())


