import ipdb
import json
import grequests
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup, Comment
from datetime import datetime
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def clickedHtml(url):
    driver = webdriver.Chrome()
    driver.get(url)
    elem = driver.find_elements_by_class_name("show-more")
    for button in elem:
        button.click()
    
    time.sleep(2)
    html = driver.page_source
    driver.close()
    
    return html

def get_pagination_quantity(soup):
    pages = soup.find('ul', {'class': 'pagin'})
    
    if pages:
        pages = pages.find_all('li')
        return pages[-2].text
    else:
        return 0

def get_filter_blocks(soup):
    try:
        soup = soup.find("div", {"class": "box-content"})

        filter_blocks_soup_name = soup.find_all('span', {"class": 'fltTtl'})
        filter_blocks_soup_hrefs = soup.find_all('ul', {'class': 'filter vis'})
        
        filter_blocks_soup_name.pop(0)
        filter_blocks_soup_hrefs.pop(0)
        
        filter_blocks = {}
        tmp = []
        counter = 0
        
        for name in filter_blocks_soup_name:
            block = filter_blocks_soup_hrefs[counter]
            
            hrefs = block.find_all('a', href=True)
            tmp = []
            
            if hrefs:
                tmp = [href['href'] for href in hrefs]
                counter += 1
            else:
                counter += 1
                continue
                
            filter_blocks[name.text] = tmp
        
        return filter_blocks
    except:
        return False

def get_current_active_filter_block(filter_blocks, name):
    for block in filter_blocks:
        if name in block.keys():
            return filter_blocks.index(block)

def get_items_quantity(soup):
    try:
        items = soup.find('ul', {'class': 'products-listing'})
        items = items.children
        result = []
        for child in items:
            if not child.attrs.get('class'):
                result.append(child)
    except:
        return 0

    return len(result)

def get_category_list(url):
    try:
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        
        req = requests.get(url, headers=headers)
        
        time.sleep(1)
        
        soup = BeautifulSoup(req.text, 'html.parser')
        soup = soup.find('ul', {'class': 'categories-listing'})
        hrefs = soup.find_all('div', {'class': 'categories-image'})
        hrefs = [href['data-link'] for href in hrefs]

        return hrefs
    except:
        return False
    
def get_info(req):
    h1_pattern = "<h1[^>]*>(.*?)<\/h1>"
    title_pattern = "<title[^>]*>(.*?)<\/title>"
    
    color_pattern = "<(.*?)><\/i>(.*?)<\/span>"
    color_pattern_second = "<(.*?)>(.*?)<\/span>"
    
    text = str(req.text)
    text = text.replace('\t', '')
    text = text.replace('\n', '')
    
    h1 = re.findall(h1_pattern, text)[0]
    
    try:
        if 'span' in h1:
            if '<i' in h1:
                find_pattern = re.findall(color_pattern, h1)
                new_color = find_pattern[0][1]
                h1 = h1.replace('<'+find_pattern[0][0]+'></i>'+new_color+'</span>', new_color)
            else:
                find_pattern = re.findall(color_pattern_second, h1)
                new_color = find_pattern[0][1]
                h1 = h1.replace('<'+find_pattern[0][0]+'>'+new_color+'</span>', new_color)
    except:
        pass
    title = re.findall(title_pattern, text)
        
    return [h1, req.url, req, title[0]]
    
def get_all_filters(one_filter_soup):
    urls_blocks = get_filter_blocks(one_filter_soup)
    
    keys = {'one_filter_data':[], 'two_filter_data':[]}
    
    if urls_blocks:
        for key in urls_blocks.keys():
            for of_url in urls_blocks[key]:
                of_url = url + of_url
                
                req = requests.get(of_url, headers=headers)
                time.sleep(1)
                soup = BeautifulSoup(req.text, 'html.parser')

                items_quantity = get_items_quantity(soup)
                
                # print('url: '+of_url+' items: '+str(items_quantity))
                if (items_quantity >= 1):
                    one_filter_data = get_info(req)
                    
                    tmp_base = {}
                    tmp_base['h1'] = one_filter_data[0]
                    tmp_base['url'] = one_filter_data[1]
                    # tmp_base['title'] = one_filter_data[3]
                    # tmp_base['pagination'] = get_pagination_quantity(soup)
                    print(tmp_base)
                    keys['one_filter_data'].append(tmp_base)
                    print("one_filters_add:", of_url)
                    
                    two_filter_soup = BeautifulSoup(one_filter_data[2].text, 'html.parser')
                    two_filter_blocks = get_filter_blocks(two_filter_soup)
                    
                    for key_two in dict(two_filter_blocks):
                        if key == key_two:
                            two_filter_blocks.pop(key_two)
                            break
                        two_filter_blocks.pop(key_two)
                    
                    for key_two in two_filter_blocks.keys():
                        for tf_url in two_filter_blocks[key_two]:
                            tf_url = url + tf_url
                            
                            tf_req = requests.get(tf_url, headers=headers)
                            time.sleep(1)
                            tf_soup = BeautifulSoup(tf_req.text, 'html.parser')
                            
                            items_quantity_two = get_items_quantity(tf_soup)
                            
                            # print('url: '+tf_url+' items: '+str(items_quantity_two))
                            
                            if ( items_quantity_two >= 1 ):
                                two_filter_data = get_info(tf_req)
                                
                                tmp = {}
                                tmp['h1'] = two_filter_data[0]
                                tmp['url'] = two_filter_data[1]
                                # tmp['title'] = two_filter_data[3]
                                # tmp['pagination'] = get_pagination_quantity(tf_soup)
                                
                                print(tmp)
                                
                                keys['two_filter_data'].append(tmp)
                                print("two_filters_add:", tf_url)
                            else:
                                print("two_filter_empty:", tf_url)
                else:
                    print("one_filter_empty:", of_url)
    else:
        return False
                
                    
    print("return time: ")
    print(datetime.now())
    return keys


print("Time start script:")
print(datetime.now())


if ( __name__ == '__main__' ):
    url = 'https://alot.com.ua'
    
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    
    filepath = 'files'
    base_categories = ['/catalog/yubki', '/zhenskaya_pizhama', '/halat_zhenskiy', '/nochnaya_rubashka', '/plus_size']
    
    # tmp = []
    # for category in base_categories:
    #     full_url = url + category
    #     sub_cat = get_category_list(full_url)
    #     if sub_cat:
    #         tmp = tmp + sub_cat
    #     else:
    #         tmp.append(category)
    
    # base_categories = tmp
    
    if base_categories:
        sub_categories_keys = {}

        for category in base_categories:
            full_url = url + category
            filename = category
            
            sub_path = filepath + filename
            
            if not os.path.isdir(sub_path):
                os.mkdir(sub_path)
            
            sub_req = requests.get(full_url, headers=headers)
            
            time.sleep(2)
            
            sub_soup = BeautifulSoup(sub_req.text, 'html.parser')
            
            filename_one = sub_path+'base_one_filter.csv'
            filename_two = sub_path+'base_two_filter.csv'

            sub_keys = get_all_filters(sub_soup)
            
            if sub_keys:
                with open(filename_one, 'w') as file:
                    fildnames = ['h1', 'url']
                    w = csv.DictWriter(file, fildnames)
                    w.writeheader()
                    w.writerows(sub_keys['one_filter_data'])
                    
                with open(filename_two, 'w') as file:
                    fildnames = ['h1', 'url']
                    w = csv.DictWriter(file, fildnames)
                    w.writeheader()
                    w.writerows(sub_keys['two_filter_data'])
                

            print("Time end script:")
            print(datetime.now())


