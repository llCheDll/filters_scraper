import ipdb
import json
import requests
import re
import csv
import os
import time

from bs4 import BeautifulSoup, Comment

def get_items_quantity(soup, url):
    try:
        soup = soup.find('div', {'class': 'products'})
        items = soup.find_all('div', {'class': 'products__list'})
        print(url + ', ' +str(len(items)))
    except:
        print(url + ', empty')

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

with open('urls.txt', 'r') as file:
    urls = file.readlines()
    
    for url in urls:
        url.replace('\n', '')
        
        req = requests.get(url, headers=headers)
            
        time.sleep(2)
            
        soup = BeautifulSoup(req.text, 'html.parser')
        
        get_items_quantity(soup, url)
        
    # ipdb.set_trace()
