import csv
import ipdb

from os import listdir
from os import path


paths = listdir('.')

base_array = []

ipdb.set_trace()

for pathfile in paths:
    try:
        if(type(pathfile) == 'str' and path.isfile(pathfile)):
            with open(path, 'rb') as file:
                csv_reader = csv.DictReader(file)
                for row in csv_reader:
                    base_array.append(row)
    except Exception as e:
        print(e)
        
with open('base_file.csv', 'w') as file:
    csv_writer = csv.DictWriter(file, fieldnames=['h1', 'url'])
    csv_writer.writeheader()
    csv_writer.writerows(base_array)
    

