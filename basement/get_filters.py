import ipdb
import json
# import grequests
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup
from datetime import datetime
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def clickedHtml(url):
    driver = webdriver.Chrome()
    driver.get(url)
    time.sleep(2)
    elem = driver.find_elements_by_tag_name("button")

    for button in elem:
        if "Показать все" in button.text :
            try:
                button.click()
                time.sleep(2)
            except:
                ipdb.set_trace()
    
    time.sleep(2)
    html = driver.page_source
    driver.close()
    
    return html

def get_pagination_quantity(url):
    req = requests.get(url)
    soup = BeautifulSoup(req.text, 'html.parser')
    pages = soup.find("div", {"class": "pages"})
    
    if pages:
        pagination_block_soup = pages.find_all('a', href=True)
        
        last = pagination_block_soup[-1]
        
        if ( 'title' not in last.attrs ):
            pagination = int(url.split('=').pop())
            return pagination
        else:
            return get_pagination_quantity(pagination_block_soup[-2].attrs['href'])
    else:
        return 0

def get_filter_blocks(soup):
    soup = soup.find_all("div", {"class": "box-content mfilter-content"})
    
    filters_urls = list()
    
    for block in soup:
        block = block.find_all('a', href=True)
        
        
        tmp = [href['href'] for href in block]
            
        filters_urls.extend(tmp)
    
    return filters_urls


def get_current_active_filter_block(filter_blocks, name):
    for block in filter_blocks:
        if name in block.keys():
            return filter_blocks.index(block)

def get_items_quantity(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    req = requests.get(url, headers=headers)
    
    time.sleep(1)
    
    soup = BeautifulSoup(req.text, 'html.parser')
    
    soup = soup.find("div", {"class": "custom-products"})
    soup = soup.find_all("div", {"class": "name"})

    return len(soup)

def get_category_list(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    req = requests.get(url, headers=headers);
        
    time.sleep(1)
    
    soup = BeautifulSoup(req.text, 'html.parser')
    soup = soup.find("div", {"class": "block1"})
    soup = soup.find_all("a", href=True)
    
    
    hrefs = [href['href'] for href in soup]

    return hrefs

    
def get_info(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    req = requests.get(url, headers=headers)
    
    try:
        h1_pattern = "<h1[^>]*>(.*?)<\/h1>"
        # title_pattern = "<title[^>]*>(.*?)<\/title>"
        # pagination_pattern = "[(]всего (.*?) страниц"
        
        text = str(req.text)
        text = text.replace('\t', '')
        text = text.replace('\n', '')
        
        h1 = re.findall(h1_pattern, text)[0]
        # title = re.findall(title_pattern, text)[0]
        # pagination = int(re.findall(pagination_pattern, text)[0])
        
        return [h1, url]
    except:
        pass

# def get_items_urls(soup):
#     soup = soup.find("div", {"class": "custom-products"})
#     soup = soup.find_all("div", {"class": "name"})
    
#     item_urls = []
#     for a_tag in soup:
#         a = a_tag.find('a', href=True)
#         item_urls.append(a['href'])
    
#     return len(item_urls)
    
def get_all_filters(one_filter_soup):
    urls_blocks = get_filter_blocks(one_filter_soup)
    
    # ipdb.set_trace()

    if urls_blocks:
        keys = {'one_filter_data':[], 'two_filter_data':[]}
        
        for url in urls_blocks:
            
            print(url)
            
            
            items_quantity = get_items_quantity(url)
            
            if (items_quantity >= 1):
                one_filter_data = get_info(url)
                
                tmp_base = {}
                tmp_base['h1'] = one_filter_data[0]
                tmp_base['url'] = one_filter_data[1]
                
                keys['one_filter_data'].append(tmp_base)
                print("one_filters_add:", url)
                pprint(tmp_base)
            else:
                print("one_filter_empty:", of_url)
                    
        print("return time: ")
        print(datetime.now())
        return keys
    else:
        return False


print("Time start script:")
print(datetime.now())


if ( __name__ == '__main__' ):
    urls = get_category_list("https://not.basement.kiev.ua/")
    
    for url in urls:
        # Create base folder for category
        filename = url.split('/')
        filename.pop()
        filename = filename.pop()
        filepath = 'files/'
        
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
        base_req = requests.get(url, headers=headers)
        time.sleep(2)
        
        soup = BeautifulSoup(base_req.text, 'html.parser')
        
        categories_keys = get_all_filters(soup)

        if categories_keys:
            with open(filepath + filename + '_filters.csv', 'w') as file:
                fildnames = ['h1', 'url']
                w = csv.DictWriter(file, fildnames)
                w.writeheader()
                w.writerows(categories_keys['one_filter_data'])
        else:
            continue
