import ipdb
import json
# import grequests
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup
from datetime import datetime
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def clickedHtml(url):
    driver = webdriver.Chrome()
    driver.get(url)
    time.sleep(2)
    elem = driver.find_elements_by_tag_name("button")

    for button in elem:
        if "Показать все" in button.text :
            try:
                button.click()
                time.sleep(2)
            except:
                ipdb.set_trace()
    
    time.sleep(2)
    html = driver.page_source
    driver.close()
    
    return html

def get_pagination_quantity(url):
    req = requests.get(url)
    soup = BeautifulSoup(req.text, 'html.parser')
    pages = soup.find("div", {"class": "pages"})
    
    if pages:
        pagination_block_soup = pages.find_all('a', href=True)
        
        last = pagination_block_soup[-1]
        
        if ( 'title' not in last.attrs ):
            pagination = int(url.split('=').pop())
            return pagination
        else:
            return get_pagination_quantity(pagination_block_soup[-2].attrs['href'])
    else:
        return 0

def get_filter_blocks(soup):
    soup = soup.find("div", {"class": "list-group"})

    if soup:
    
        filter_blocks_soup = soup.find_all("div", {"class": "list-group-item ocfilter-option"})

        filter_blocks = {}
        tmp = []
        counter = 0
        
        for bad_block in filter_blocks_soup:
            name = bad_block.find("div", {"class": "ocf-option-name"}).text.strip()
            hrefs = bad_block.find_all("label")
            
            if hrefs:
                tmp = [href.a['href'] for href in hrefs]
                
            filter_blocks[name] = tmp
                            
        return filter_blocks
    else:
        return False

def get_current_active_filter_block(filter_blocks, name):
    for block in filter_blocks:
        if name in block.keys():
            return filter_blocks.index(block)

def get_items_quantity(url):
    req = requests.get(url)
    
    time.sleep(1)
    
    soup = BeautifulSoup(req.text, 'html.parser')
    
    items = soup.find_all(
        "div", {"class": "product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12"}
        )

    return len(items)

def get_category_list(url):
    
    req = requests.get(url)
        
    time.sleep(1)
    
    soup = BeautifulSoup(req.text, 'html.parser')
    soup = soup.find_all("div", {"class": "col-sm-6 col-md-4 col-lg-3"})
    
    hrefs = [href.a['href'] for href in soup]

    return hrefs

    
def get_info(url):
    req = requests.get(url)
    
    try:
        h1_pattern = "<h1[^>]*>(.*?)<\/h1>"
        title_pattern = "<title[^>]*>(.*?)<\/title>"
        pagination_pattern = "[(]всего (.*?) страниц"
        
        text = str(req.text)
        text = text.replace('\t', '')
        text = text.replace('\n', '')
        
        h1 = re.findall(h1_pattern, text)[0]
        title = re.findall(title_pattern, text)[0]
        pagination = int(re.findall(pagination_pattern, text)[0])
        
        return [h1, url, req, title, pagination]
    except:
        pass
    
def get_all_filters(one_filter_soup):
    urls_blocks = get_filter_blocks(one_filter_soup)
    if urls_blocks:
        keys = {'one_filter_data':[], 'two_filter_data':[]}
        
        for key in urls_blocks.keys():
            for of_url in urls_blocks[key]:
                print(of_url)
                
                items_quantity = get_items_quantity(of_url)
                
                if (items_quantity > 3):
                    one_filter_data = get_info(of_url)
                    
                    tmp_base = {}
                    tmp_base['h1'] = one_filter_data[0]
                    tmp_base['url'] = one_filter_data[1]
                    tmp_base['title'] = one_filter_data[3]
                    tmp_base['pagination'] = one_filter_data[4]
                    
                    keys['one_filter_data'].append(tmp_base)
                    print("one_filters_add:", of_url)
                    pprint(tmp_base)
                    
                    two_filter_soup = BeautifulSoup(one_filter_data[2].text, 'html.parser')
                    two_filter_blocks = get_filter_blocks(two_filter_soup)
                    
                    for key_two in dict(two_filter_blocks):
                        if key == key_two:
                            two_filter_blocks.pop(key_two)
                            break
                        two_filter_blocks.pop(key_two)
                    
                    for key_two in two_filter_blocks.keys():
                        for tf_url in two_filter_blocks[key_two]:
                            print(tf_url)
                            
                            items_quantity_two = get_items_quantity(tf_url)
                            
                            if ( items_quantity_two > 3 ):
                                two_filter_data = get_info(tf_url)
                                
                                tmp = {}
                                tmp['h1'] = two_filter_data[0]
                                tmp['url'] = two_filter_data[1]
                                tmp['title'] = two_filter_data[3]
                                tmp['pagination'] = two_filter_data[4]
                                
                                keys['two_filter_data'].append(tmp)
                                print("two_filters_add:", tf_url)
                            else:
                                print("two_filter_empty:", tf_url)
                else:
                    print("one_filter_empty:", of_url)
                    
                        
        print("return time: ")
        print(datetime.now())
        return keys
    else:
        return False


print("Time start script:")
print(datetime.now())


if ( __name__ == '__main__' ):
    urls = get_category_list("https://vorota-shop.com.ua/vorota-ih-tipy-i-cena")
    
    # urls_subcat = []
    
    # for url in urls:
    #     urls_subcat.append(get_category_list(url))
    
    # end_url = urls_subcat
    
    # for urls_subcat_base in urls_subcat:
    #     for url in urls_subcat_base:
    #         end_url.append(get_category_list(url))
            

    # urls = [x for x in end_url if x]
    # urls = [y for x in end_urls for y in x]
    
    # ipdb.set_trace()
    
    for url in urls:
        # Create base folder for category
        filepath = url.split('/')
        filepath = filepath.pop()
        filepath = 'files/' + 'vorota-ih-tipy-i-cena' + '/' + filepath
        
        if not os.path.isdir(filepath):
            if not os.path.isdir('files/' + 'vorota-ih-tipy-i-cena' + '/'):
                os.mkdir('files/' + 'vorota-ih-tipy-i-cena' + '/')
            os.mkdir(filepath)
            
        base_req = requests.get(url)
        # base_req = clickedHtml(url)
        time.sleep(2)
        
        base_soup = BeautifulSoup(base_req.text, 'html.parser')
        
        base_categories_keys = get_all_filters(base_soup)

        if base_categories_keys:
            with open(filepath + 'base_one_filters.csv', 'w') as file:
                fildnames = ['h1', 'title', 'url', 'pagination']
                w = csv.DictWriter(file, fildnames)
                w.writeheader()
                w.writerows(base_categories_keys['one_filter_data'])

            with open(filepath + 'base_two_filters.csv', 'w') as file:
                fildnames = ['h1', 'title', 'url', 'pagination']
                w = csv.DictWriter(file, fildnames)
                w.writeheader()
                w.writerows(base_categories_keys['two_filter_data'])
        else:
            continue

        # base_categories = get_category_list(url)
        # if base_categories:
        #     sub_categories = [url['href'] for url in base_categories]
        #     sub_categories_keys = {}

        #     for category in sub_categories:
        #         if 'javascript:void(0)' not in category:
        #             filename = category.split('/')
        #             filename = filename.pop().split('.')[0]
                    
        #             sub_path = filepath + filename
                    
        #             if not os.path.isdir(sub_path):
        #                 os.mkdir(sub_path)
                    
        #             sub_req = requests.get(category)
                    
        #             time.sleep(2)
                    
        #             sub_soup = BeautifulSoup(sub_req.text, 'html.parser')
                    
        #             filename_one = sub_path+'/base_one_filter.csv'
        #             filename_two = sub_path+'/base_two_filter.csv'

        #             sub_keys = get_all_filters(sub_soup)
                    
        #             if sub_keys:
        #                 with open(filename_one, 'w') as file:
        #                     fildnames = ['h1', 'title', 'url', 'pagination']
        #                     w = csv.DictWriter(file, fildnames)
        #                     w.writeheader()
        #                     w.writerows(sub_keys['one_filter_data'])
                            
        #                 with open(filename_two, 'w') as file:
        #                     fildnames = ['h1', 'title', 'url', 'pagination']
        #                     w = csv.DictWriter(file, fildnames)
        #                     w.writeheader()
        #                     w.writerows(sub_keys['two_filter_data'])
                        
        #                 sub_sub_soup = get_category_list(category)
                        
        #                 if sub_sub_soup:
        #                     end_categories = [url['href'] for url in sub_sub_soup]
                        
        #                     end_categories_keys = {}
        #                     for end_category in end_categories:
        #                         filename_end = end_category.split('/')
        #                         filename_end = filename_end.pop().split('.')[0]
                                
        #                         end_req = requests.get(end_category)
        #                         end_soup = BeautifulSoup(end_req.text, 'html.parser')
        #                         end_keys = get_all_filters(end_soup)
        #                         # end_req.
                                
        #                         filename_one = sub_path+'/'+filename_end+'_one_filter.csv'
        #                         filename_two = sub_path+'/'+filename_end+'_two_filter.csv'
                                
        #                         with open(filename_one, 'w') as file:
        #                             fildnames = ['h1', 'title', 'url', 'pagination']
        #                             w = csv.DictWriter(file, fildnames)
        #                             w.writeheader()
        #                             w.writerows(end_keys['one_filter_data'])
                                    
        #                         with open(filename_two, 'w') as file:
        #                             fildnames = ['h1', 'title', 'url', 'pagination']
        #                             w = csv.DictWriter(file, fildnames)
        #                             w.writeheader()
        #                             w.writerows(end_keys['two_filter_data'])

        #         print("Time end script:")
        #         print(datetime.now())


