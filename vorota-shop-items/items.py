import ipdb
import json
# import grequests
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup
from datetime import datetime
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def get_info(req):
    # ipdb.set_trace()
    
    h1_pattern = "<h1[^>]*>(.*?)<\/h1>"
    meta_robots_pattern = "<meta[^>]*?name=[\'\"]?robots[\'\"] content=[\'\"](.*)*?[\'\"]>"
    url_pattern = "<link href=[\'\"](.*?)[\'\"] rel=[\'\"]canonical[\'\"] \/>"
    
    text = str(req.text)
    text = text.replace('\t', '')
    text = text.replace('\n', '')
    meta_robots = re.findall(meta_robots_pattern, text)
    h1 = re.findall(h1_pattern, text)
    url = re.findall(url_pattern, text)
    
    if(meta_robots):
        meta_robots = meta_robots[0]
    else:
        meta_robots = '-'
    
    return {'h1':h1[0], 'url':url[0], 'robots':meta_robots}

def get_items_urls(soup):
    
    soup = soup.find_all("div", {"class": "product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12"})
    
    item_urls = []
    
    for item in soup:
        item_href = item.find("a", {"itemprop": "name"})['href']
        item_urls.append(item_href)
    
    return item_urls

def get_categories(soup):
    soup = soup.find("ul", {"class": "menu"})
    soup = soup.find_all("a", href=True)
    
    categories = [href['href'] for href in soup]
    
    return categories


if ( __name__ == '__main__' ):
    urls = ["https://vorota-shop.com.ua/bollardy-dorozhnye-blokiratory"]
    items_urls = [
        "https://vorota-shop.com.ua/bollardy-dorozhnye-blokiratory"
        # "https://www.spacevr.com.ua/category/questsvr/page/",
        # "https://www.spacevr.com.ua/category/events/page/",
        # "https://www.spacevr.com.ua/category/arenda-oborudovaniya/page/",
        # "https://www.spacevr.com.ua/category/podarochnyye-sertifikaty/page/"
    ]
    
    counter = 1
    pagee = 1
    
    info = []
    
    url = urls[0]
    
    filepath = 'files/'
    
    item_urls = []
    
    for pages in range(counter, 100):
        if (counter == 1):
            url = urls[0]
        else:
            url = urls[0] + '?page=' + str(counter)
        
        base_req = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
        
        page = base_req.url.split('=')
        
        if (len(page) >= 2):
            page = int(page.pop())
        else:
            page = 1
            
        print(page)
            
        if (counter > page):
            break
            
        print(base_req.url)
            
        base_soup = BeautifulSoup(base_req.text, 'html.parser')
        
        for href in get_items_urls(base_soup): item_urls.append(href)
        
        counter += 1
        
    scraped_urls = []

    info = []
    
    for href in item_urls:
        try:
            print('ADD: ' + href)
            s = requests.Session()
            s.headers['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36'
            href_req = s.get(href, allow_redirects=False)
            inf = get_info(href_req)
            info.append(inf)
        except Exception as e:
            print(e)
            ipdb.set_trace()
            print('SKIP: ' + href)

    with open('files/items.csv', 'w') as file:
        fildnames = ['h1', 'url', 'robots']
        w = csv.DictWriter(file, fildnames)
        w.writeheader()
        w.writerows(info)
    
    print(info)
    
    print("Time end script:")
    print(datetime.now())


