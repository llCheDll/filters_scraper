import ipdb
import json
import grequests
import requests
import re
import csv
import time
import os

from bs4 import BeautifulSoup
from datetime import datetime
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def clickedHtml(url):
    driver = webdriver.Chrome()
    driver.get(url)
    elem = driver.find_elements_by_class_name("show-more")
    for button in elem:
        button.click()
    
    time.sleep(2)
    html = driver.page_source
    driver.close()
    
    return html

def get_filter_blocks(soup):
    soup = soup.find("div", {"class": "block_fv attrb"})

    filter_blocks_soup = soup.find_all('div', {"class": "block_param"})

    filter_blocks = []

    for block in filter_blocks_soup:
        hrefs = block.find_all('a', href=True)
        filter_blocks.append(hrefs)
        
    return filter_blocks
    
    
def sitemap_get_filter(filter_blocks, filter_quantity):
    sitemapOF = {}
    urls_block = []
    
    # if (filter_quantity == 'two'):
        # filter_blocks.pop(0)
    
    for block in filter_blocks:
        urls = []
        for href in block:
            urls.append(href['href'])
        urls_block.append(urls)
    return urls_block
    
        

def get_info(url):
    req = requests.get(url)
    try:
    
        pattern = "<h1[^>]*>(.*?)<\/h1>"
        
        text = str(req.text)
        text = text.replace('\t', '')
        text = text.replace('\n', '')
        
        h1 = re.findall(pattern, text)
        
        return [h1[0], url, req]
    except:
        print(req.text)
    
    
def get_all_filters(one_filter_soup):
    one_filter_blocks = get_filter_blocks(one_filter_soup)
    one_urls_block = sitemap_get_filter(one_filter_blocks, 'one')
    keys = {'one_filter_data':[], 'two_filter_data':[]}
    
    counter = 0
    
    for one_filter_urls in one_urls_block:
        for of_url in one_filter_urls:
            print(of_url)
            one_filter_data = get_info(of_url)
            
            tmp_base = {}
            tmp_base['h1'] = one_filter_data[0]
            tmp_base['url'] = one_filter_data[1]
            
            keys['one_filter_data'].append(tmp_base)
            
            if( counter < len(one_urls_block)-1 ):
                two_filter_soup = BeautifulSoup(one_filter_data[2].text, 'html.parser')
                two_filter_blocks = get_filter_blocks(two_filter_soup)
                two_urls_block = sitemap_get_filter(two_filter_blocks[counter+1:], 'two')
                
                for two_filter_urls in two_urls_block:
                    for tf_url in two_filter_urls:
                        two_filter_data = get_info(tf_url)
                        
                        tmp = {}
                        tmp['h1'] = two_filter_data[0]
                        tmp['url'] = two_filter_data[1]
                        
                        keys['two_filter_data'].append(tmp)
                
        counter += 1
    print("return time: ")
    print(datetime.now())
    return keys


print("Time start script:")
print(datetime.now())

# rs = (grequests.get(u) for u in urls)


# soup = BeautifulSoup(response[0].text, 'html.parser')

# keys = get_all_filters(soup)

# with open('files/dump.json', 'w', encoding='utf8') as file:
#     json.dump(keys, file, indent=4, ensure_ascii=False)

url = "https://greenhol.ua/besfosfatnaya-bytovaya-himiya/"
base_req = requests.get(url)
filepath = url.split('/')
filepath.pop()
filepath = 'files/' + filepath.pop() + '/'

# get base category keys
base_soup = BeautifulSoup(base_req.text, 'html.parser')
base_categories_keys = get_all_filters(base_soup)

os.mkdir(filepath)

with open(filepath + 'base_one_filters.csv', 'w') as file:
    fildnames = ['h1', 'url']
    w = csv.DictWriter(file, fildnames)
    w.writeheader()
    w.writerows(base_categories_keys['one_filter_data'])

with open(filepath + 'base_two_filters.csv', 'w') as file:
    fildnames = ['h1', 'url']
    w = csv.DictWriter(file, fildnames)
    w.writeheader()
    w.writerows(base_categories_keys['two_filter_data'])


base_categories = base_soup.find('div', {'class': 'cat-list'})
base_categories = base_categories.find_all('a', href=True)

sub_categories = [url['href'] for url in base_categories]
sub_categories_keys = {}

for category in sub_categories:
    filename = category.split('/')
    filename.pop()
    filename = filename.pop()
    
    sub_path = filepath + filename
    os.mkdir(sub_path)
    
    sub_req = requests.get(category)
    sub_soup = BeautifulSoup(sub_req.text, 'html.parser')
    
    filename_one = sub_path+'/base_one_filter.csv'
    filename_two = sub_path+'/base_two_filter.csv'

    sub_keys = get_all_filters(sub_soup)

    with open(filename_one, 'w') as file:
        fildnames = ['h1', 'url']
        w = csv.DictWriter(file, fildnames)
        w.writeheader()
        w.writerows(sub_keys['one_filter_data'])
        
    with open(filename_two, 'w') as file:
        fildnames = ['h1', 'url']
        w = csv.DictWriter(file, fildnames)
        w.writeheader()
        w.writerows(sub_keys['two_filter_data'])
    
    sub_categories_keys[category] = sub_keys
    
    sub_sub_soup = sub_soup.find('div', {'class': 'cat-list'})
    
    if(sub_sub_soup):
        sub_sub_soup = sub_sub_soup.find_all('a', href=True)
        end_categories = [url['href'] for url in sub_sub_soup]
    
        end_categories_keys = {}
        for end_category in end_categories:
            filename_end = end_category.split('/')
            filename_end.pop()
            filename_end = filename_end.pop()
        
            end_req = requests.get(end_category)
            end_soup = BeautifulSoup(end_req.text, 'html.parser')
            end_keys = get_all_filters(end_soup)
            # end_req.
            
            filename_one = sub_path+'/'+filename_end+'_one_filter.csv'
            filename_two = sub_path+'/'+filename_end+'_two_filter.csv'
            
            with open(filename_one, 'w') as file:
                fildnames = ['h1', 'url']
                w = csv.DictWriter(file, fildnames)
                w.writeheader()
                w.writerows(end_keys['one_filter_data'])
                
            with open(filename_two, 'w') as file:
                fildnames = ['h1', 'url']
                w = csv.DictWriter(file, fildnames)
                w.writeheader()
                w.writerows(end_keys['two_filter_data'])

print("Time end script:")
print(datetime.now())


